class MortgageCalcController < ApplicationController

  def details
  end

  def results
    @loan_amount = params[:loan_amount]
    @term_length = params[:term_length]
    @interest_rate = params[:interest_rate]
    raise "Loan amount must have a value" unless @loan_amount
    @monthly_payment = calculate_monthly_payment(@loan_amount.to_f, @term_length.to_f, @interest_rate.to_f)
  end

  # The formula for calculating a fixed monthly payment for a loan is:
  #     P = L[c(1 + c)^n]/[(1 + c)^n - 1]
  #
  # where
  #  L is the Loan Amount
  #  c is the monthly interest rate (divide by 12)
  #  n is the number of months
  #
  # params given:
  #  loan_amount
  #  term_length is term length in years
  #  annual_interest_rate_perc is annual interest rate as a percentage
  def calculate_monthly_payment(loan_amount, term_length, annual_interest_rate_perc)
    number_of_months = term_length * 12
    monthly_interest_rate = annual_interest_rate_perc / 100 / 12
    one_plus_c_to_n = (1 + monthly_interest_rate) ** number_of_months
    (loan_amount * (monthly_interest_rate * one_plus_c_to_n) / (one_plus_c_to_n - 1)).round(2)
  end

end
