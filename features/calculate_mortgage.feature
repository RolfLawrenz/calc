Feature: Calculate Mortgages

  Scenario: A user calculates mortgage with valid values
    Given I am on the mortgage calc details page
    When I enter a "loan amount" of "100000"
    And I enter a "term length" of "30"
    And I enter a "interest rate" of "5"
    And I press "Calculate"
    Then I should see "Monthly Payment"
    And I should see "536.82"

  Scenario: A user calculates mortgage with valid values and returns to details page
    Given I am on the mortgage calc details page
    And I enter a "loan amount" of "100000"
    And I enter a "term length" of "30"
    And I enter a "interest rate" of "5"
    And I press "Calculate"
    When I click "Re enter"
    Then I should see "Mortgage Calculator Details"

