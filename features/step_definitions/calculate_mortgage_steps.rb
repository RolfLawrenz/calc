require File.expand_path(File.join(File.dirname(__FILE__), "..", "support", "paths"))

Given(/^I am on the (.+)$/) do |page_name|
  visit path_to(page_name)
end

When(/^I enter a "(.*?)" of "(.*?)"$/) do |field, value|
  fill_in(field.gsub(' ','_'), :with => value)
end

When(/^I press "(.*?)"$/) do |button|
  click_button(button)
end

Then(/^the "(.*?)" field should contain "(.*?)"$/) do |field, value|
  find_field(field).value.should =~ /#{value}/
end

Then(/^I should see "(.*?)"$/) do |exp|
  regexp = Regexp.new(exp)
  expect(page).to have_content(regexp)
end

When(/^I click "(.*?)"$/) do |link|
  click_link(link)
end
