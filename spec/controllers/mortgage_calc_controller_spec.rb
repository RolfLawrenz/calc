require 'rails_helper'

RSpec.describe MortgageCalcController, :type => :controller do

  describe "#calculate_monthly_payment" do

    it "should return correct monthly payment" do
      expect(controller.calculate_monthly_payment(100000.0, 30.0, 5.0)).to eq(536.82)
    end

  end

end
